{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Building a Link Crawler with Streams\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Building web site crawlers is a reasonably well-understood problem: a program (aka \"a bot\") visits a URL, reads in the information on the page, extracts things it finds useful, finds links it wants to follow, and then repeats until it decides it is done.\n",
    "\n",
    "Various solutions, libraries, and products exist to help you set one up and run it. But have you stopped to think about how they work? Could you build your own? If you could, would that help you understand more about your site, your servers, and how crawlers interact with them?\n",
    "\n",
    "In this guide, we'll use JavaScript and a concept called \"streams\" to build our own crawler.\n",
    "\n",
    "First, let's bring in the libraries we'll use:\n",
    "\n",
    "- [Highland](http://highlandjs.org/): streams processor and an API to make them easier to work with\n",
    "- [Cheerio.js](https://cheerio.js.org/): HTML processor\n",
    "- [http](https://nodejs.org/api/http.html)/[https](https://nodejs.org/api/https.html): HTTP and HTTPS request modules from the Node.js standard library\n",
    "- [Node CSV](http://csv.adaltas.com/): helps us turn JSON data into CSV data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const highland = require('highland');\n",
    "const cheerio = require('cheerio');\n",
    "const http = require('http');\n",
    "const https = require('https');\n",
    "const stringify = require('csv-stringify/lib/es5');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start by making a list of URLs we want to crawl:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const URLS = [ 'https://moz.com' ];"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Great. Let's continue by setting up a stream we can add URLs to and then consume for processing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const crawlStream = highland();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Super simple. We want to treat this like the source of our data stream, but it's sort of empty.\n",
    "Well according to the [Highland `write` documentation](https://highlandjs.org/#write), an empty Highland object is a writable stream.\n",
    "\n",
    "So that means we can add our known beginning URLs as well as add new ones as we discover them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "URLS.forEach(url => crawlStream.write(url));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## High-Level Plan\n",
    "\n",
    "So let's think about the high-level steps we need to do with this stream:\n",
    "\n",
    "1. download the HTML for each URL\n",
    "2. parse each HTML body and extract all the links\n",
    "3. for each link that is one of our target links, send it downstream for processing and storage\n",
    "4. for each link that is an internal link in our site that we haven't already seen, send it back to step 1 and start crawling"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Downloading and Extracting Links\n",
    "\n",
    "Let's build steps 1 and 2 with Highland, the http/https modules, and Cheerio.\n",
    "\n",
    "First we'll set up some helpers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "/**\n",
    "HTTP requests are asynchronous in Node. That means the code\n",
    "won't wait for the response to come back before moving on.\n",
    "Instead, we need to wait for a callback. But how do we pass\n",
    "that through the stream? Instead of the direct results, we\n",
    "can return a \"Promise\" for the eventual result to the stream.\n",
    "Highland knows how to process wrapped Promises.\n",
    "\n",
    "This results in a \"stream of streams\".\n",
    "*/\n",
    "function downloadSite(sourceUrl) {\n",
    "    // Node has different modules depending on the protocol.\n",
    "    // Here we look to see if the URL starts with https and\n",
    "    // selects the right module.\n",
    "    // Both have a \"get\" function to start the request\n",
    "    const fn = (/^https/.test(sourceUrl) ? https : http).get;\n",
    "    const processResponse = (resolve, reject) => {\n",
    "        return fn(sourceUrl, res => {\n",
    "            // We need to indicate that a request failed and send\n",
    "            // these errors downstream to be processed later.\n",
    "            if (res.statusCode >= 400) {\n",
    "                return reject(\n",
    "                    new Error(\n",
    "                        `request to ${sourceUrl} failed with ${res.statusCode}`\n",
    "                    )\n",
    "                );\n",
    "            }\n",
    "\n",
    "            // Node response objects are EventEmitters, and we \n",
    "            // can those with Highland too and treat them like\n",
    "            // streams. We want to end up with an object containing\n",
    "            // the source URL and site contents.\n",
    "            res.setEncoding('utf8');\n",
    "            // Here we collect the data chunks into a final body object\n",
    "            // and then tack on the original URL with the results.\n",
    "            let body = '';\n",
    "            res.on('data', chunk => body = body + chunk);\n",
    "            res.on('end', () => resolve({ body, sourceUrl }));      \n",
    "        });\n",
    "    };\n",
    "    \n",
    "    return highland(new Promise(processResponse));\n",
    "}\n",
    "\n",
    "/**\n",
    "Each time a downloaded site comes into the stream, we\n",
    "parse it with cheerio to extract all the links. We can\n",
    "return this as a stream of link objects to process\n",
    "downstream.\n",
    "*/\n",
    "function extractLinks({ body, sourceUrl } = {}) {\n",
    "    body = body || '';\n",
    "    const parser = cheerio.load(body);\n",
    "    const links = parser('a').\n",
    "        map((index, link) => {\n",
    "            const $link = cheerio(link);\n",
    "            return { \n",
    "                sourceUrl, \n",
    "                href: $link.attr('href'), \n",
    "                text: $link.text(),\n",
    "            };\n",
    "        });\n",
    "    return highland(links.get());\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Highland has a really great chaining API, which means we can\n",
    "add functions to modify streams as data goes through the chain.\n",
    "Each chained function returns a new stream, so let's update our\n",
    "stream reference with these new data changes and compose our helper\n",
    "functions into the stream."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const links = crawlStream.\n",
    "    tap((url) => console.log(`Processing ${url}...`)).\n",
    "    // As mentioned, the code that downloads site contents returns\n",
    "    // a stream of streams for the content. We use \"flatMap\" to\n",
    "    // return it to a more linear stream that is easier to think about.\n",
    "    flatMap(downloadSite).\n",
    "    flatMap(extractLinks);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dividing Up Links\n",
    "\n",
    "Whoo! That was a lot. But at this point we should have a stream of links flowing through our pipeline.\n",
    "\n",
    "Let's think about steps 3 and 4. So far we have only seen streams processing flowing as a single line: one source, one destination, and processing functions stacked in a line.\n",
    "\n",
    "But we want to process internal and target links differently. So what do we do?\n",
    "\n",
    "We can split the streams and deal with two logically distinct streams with separate behavior.\n",
    "\n",
    "In Highland, this is called [forking](http://highlandjs.org/#fork) and [observing](http://highlandjs.org/#observe).\n",
    "\n",
    "Explaining these concepts are a bit involved for the purposes of this guide, so for now just know that these also let our consumer streams communicate with our producer streams to manage how fast to send data through the pipeline.\n",
    "\n",
    "Let's set up some helpers to know how to tell links apart:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const INTERNAL_PATTERN = /moz.com\\/(?!researchtools\\/ose)/;\n",
    "const TARGET_PATTERN = /moz.com\\/researchtools\\/ose/;\n",
    "\n",
    "function isInternalLink ({ href } = {}) {\n",
    "    return INTERNAL_PATTERN.test(href);\n",
    "}\n",
    "\n",
    "function isTargetLink ({ href } = {}) {\n",
    "    return TARGET_PATTERN.test(href);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, armed with those helpers, let's create 2 distinct streams of links."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const internalLinks = links.\n",
    "    fork().\n",
    "    filter(isInternalLink);\n",
    "\n",
    "const targetLinks = links.\n",
    "    observe().\n",
    "    filter(isTargetLink);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Self-Feeding Stream\n",
    "\n",
    "For a crawler to work, we need to follow internal links and recursively descend through the tree of links. We also want to keep track of where we've been so we don't follow loop cycles endlessly.\n",
    "\n",
    "Do do that, we want to keep track of what we've seen and only let new links through.\n",
    "\n",
    "Since we're dealing with a stream that started with a stream representing an initial set URLs, how can we add to that stream with URLs we didn't know about ahead of time?\n",
    "\n",
    "A Highland stream is both readable and writable, so let's have this stream write back to the top of the stream to start the process all over again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const url = require('url');\n",
    "\n",
    "/**\n",
    "We shouldn't merely compare URL strings to determine uniqueness.\n",
    "Link URLs can include different protocols, query paramaters, hashes,\n",
    "and so on. Let's just slim down to the URL host and path and\n",
    "track visited links that way.\n",
    "*/\n",
    "function compareLinkUrls (left, right) {\n",
    "    const l = url.parse(left.href);\n",
    "    const r = url.parse(right.href);\n",
    "    \n",
    "    const hostMatch = l.host === r.host;\n",
    "    const pathMatch = l.pathname === r.pathname;\n",
    "\n",
    "    return hostMatch && pathMatch;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we can adjust our links stream to only let links through that we haven't already visted:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "internalLinks.\n",
    "    uniqBy(compareLinkUrls).\n",
    "    pluck('href').\n",
    "    pipe(crawlStream)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Processing and Storing Results\n",
    "\n",
    "This stream adventure is fun, but not everyone in the office deals with programs, databases, and data streams.\n",
    "We need to get this data into a format we can save to a file and share around.\n",
    "\n",
    "Enter CSV, the lingua franca of data processing and collaboration outside of engineering contexts.\n",
    "\n",
    "We don't want to spend our brainpower generating CSV formats. Let's make a library do it for us and then send it to a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const { createWriteStream } = require('fs');\n",
    "const dest = createWriteStream('./results.csv', 'utf8');\n",
    "dest.on('finish', () => {\n",
    "    console.log('Done!');\n",
    "});\n",
    "\n",
    "const csvWriter = stringify({ columns: [ 'sourceUrl', 'href', 'text' ] });\n",
    "\n",
    "targetLinks.\n",
    "    pipe(csvWriter).\n",
    "    pipe(dest);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Putting it all Together\n",
    "\n",
    "I broke up the stream objects into smaller steps to make things easier to read.\n",
    "Let's jam it all together into one big chain to see how it runs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const source = highland();\n",
    "\n",
    "// Extract links from sites that come into the stream\n",
    "const links = source.\n",
    "    tap(url => console.log(`Processing ${url}...`)).\n",
    "    flatMap(downloadSite).\n",
    "    errors(err => console.error(err)).\n",
    "    flatMap(extractLinks);\n",
    "\n",
    "// Set up internal links loop\n",
    "links.\n",
    "    fork().\n",
    "    filter(isInternalLink).\n",
    "    uniqBy(compareLinkUrls).\n",
    "    pluck('href').\n",
    "    each(url => source.write(url));\n",
    "    //pipe(source);\n",
    "\n",
    "// Set up CSV transformer and output destination\n",
    "const writer = stringify({ columns: [ 'sourceUrl', 'href', 'text' ] });\n",
    "const out = require('fs').createWriteStream('./results.csv', 'utf8');\n",
    "out.on('finish', () => {\n",
    "    console.log('Done!');\n",
    "});\n",
    "\n",
    "// Set up target links stream processor\n",
    "links.\n",
    "    observe().\n",
    "    filter(isTargetLink).\n",
    "    pipe(writer).\n",
    "    pipe(out);\n",
    "\n",
    "// Kick off the stream!\n",
    "URLS.forEach(url => source.write(url));\n",
    "'start stream'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should stream results to a file called `results.csv` in the `notebooks` folder of this project. Open another terminal window, navigate to the project, and find the file. You can watch the crawler add results to the file as it crawls with:\n",
    "\n",
    "    tail -f results.csv\n",
    "    \n",
    "Hit `Ctrl-C` to stop following the file. You can either let the crawler run through to completion, or hit the Stop button (the black square in the Notebook toolbar) to stop crawling."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wrapping Up\n",
    "\n",
    "So far we showed how you can use important and simple concepts to build big things:\n",
    "\n",
    "- data as streams\n",
    "- streams can be split, transformed, piped around\n",
    "- streams can be treated like code and passed around\n",
    "\n",
    "There are a few aspects of crawlers we didn't touch:\n",
    "\n",
    "- respecting `nofollow` and `nofollow` links\n",
    "- respecting `robots.txt`\n",
    "- filtering patterns we don't want to follow (maybe we don't want to go to the community page)\n",
    "- filtering parts of the page we do or don't care about (do we want to process footer links on every page?)\n",
    "\n",
    "Given what you know and stream tools like [filter](https://highlandjs.org/#filter), [reject](https://highlandjs.org/#reject), and [map](https://highlandjs.org/#map), can you customize this crawler for your needs?\n",
    "\n",
    "Another aspect we didn't cover is performance and error handling.\n",
    "\n",
    "How long do you think each individual data processing step takes above? A stream is as fast as it slowest step, so what can you do to speed this up for very large sites? If you know most of your time is stuck waiting for sites do download, could you divide up the download work to run in parallel?\n",
    "\n",
    "What should happen if a URL gives you a `400` or `404` status response? What about a `500`? Should you try again? What happens if you get rate limited? Can you tell?\n",
    "\n",
    "There are answers in Highland for some of these. But at this point, we've escaped the simplifications and assumptions we've made for the sake of learning.\n",
    "\n",
    "But hopefully you're left with a better understanding of how this works.\n",
    "\n",
    "Cheers!\n",
    "\n",
    "--David"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jp-Babel (Node.js)",
   "language": "babel",
   "name": "babel"
  },
  "language_info": {
   "file_extension": ".js",
   "mimetype": "application/javascript",
   "name": "javascript",
   "version": "4.2.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
