# Building a Link Crawler with Streams

Ever tried to build a site crawler? Want to extract links based on rules and
either save or crawl them? Wish you could program your own crawler but you
don't feel comfortable with the underlying programming concepts?

This project contains the setup and content for a [Jupyter
Notebook](http://jupyter.org/) to walk you through it using JavaScript and
Streams.

If you aren't already familiar with Jupyter Notebooks, they're basically
programmable and runnable documentation and guides. You can learn how streams
work, how to use them to download websites, extract links, keep the ones you
want, and either save or crawl them.

The best part is you can play with the bits of code in the guide and even run
them right within the content. And then at the end, you're left with a crawler
that you can run right from the notebook.

## Prerequisites

The files in this repo are the notebook content itself and the configuration and
scripts to set up the notebook. This doesn't include the Jupyter Notebook engine
and the JavaScript kernel itself.

Instead, it uses a technology called Docker to use the image (think of it like a
"recipe") to build a container--basically a small, self-contained, isolated
program environment--running the engine and your notebook.

Here is what you need to have installed on your computer to get started:

- [Docker](https://www.docker.com/community-edition)
- [Node.js](https://nodejs.org/en/download/)

## Running

Once you have that set up, download this repository to a folder on your computer
and navigate to it with a terminal program.

If you have `git` on your computer you can run:

    git clone https://gitlab.com/TheDahv/link-crawl-stream.git
    cd link-crawl-stream

Then run the script to boot up the notebook:

    ./build-and-run.sh

You should see terminal output telling you what it is installing, building, and
executing. At the end, you should see Jupyter booting up and telling you how to
access it in the browser. Here is an example from my computer (your URL will
look slightly different):

    [I 16:52:09.619 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
    [W 16:52:10.005 NotebookApp] WARNING: The notebook server is listening on all IP addresses and not using encryption. This is not recommended.
    [I 16:52:10.015 NotebookApp] Serving notebooks from local directory: /opt/notebooks
    [I 16:52:10.015 NotebookApp] 0 active kernels
    [I 16:52:10.015 NotebookApp] The Jupyter Notebook is running at:
    [I 16:52:10.016 NotebookApp] http://[all ip addresses on your system]:8888/?token=7d2c6d26766c3eb7e20c8061914c88cda98e57cccb1a99db
    [I 16:52:10.016 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
    [C 16:52:10.016 NotebookApp]

        Copy/paste this URL into your browser when you connect for the first time,
        to login with a token:
            http://localhost:8888/?token=7d2c6d26766c3eb7e20c8061914c88cda98e57cccb1a99db

Open that in the browser and start reading. 

You can use `shift-Enter` to execute the code blocks as you read. Have fun!
